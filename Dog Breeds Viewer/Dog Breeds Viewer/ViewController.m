//
//  ViewController.m
//  Dog Breeds Viewer
//
//  Created by ingenuity on 09/08/2017.
//  Copyright © 2017 ingenuity. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSError *error;
    NSString *filename = [[NSBundle mainBundle] pathForResource:@"breeds" ofType:@"json"];
    if (filename) {
        NSLog(@"breeds.json found!");
        NSData *breedsData = [[NSData alloc] initWithContentsOfFile:filename];
        NSDictionary *breeds = [NSJSONSerialization JSONObjectWithData:breedsData
                                                               options:0
                                                                 error:&error];
        if (!error) {
            NSLog(@"breedsInfo: %@", breeds);
        }
    } else {
        NSLog(@"breeds.json not found!");
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
