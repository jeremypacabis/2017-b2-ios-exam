//
//  Breed.h
//  Dog Breeds Viewer
//
//  Created by ingenuity on 09/08/2017.
//  Copyright © 2017 ingenuity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Breed : NSObject

@property NSString *breed;
@property NSString *personality;
@property NSString *energy_level;
@property NSString *good_with_children;
@property NSString *good_with_other_dogs;
@property NSString *shedding;
@property NSString *grooming;
@property NSString *trainability;
@property NSString *height;
@property NSString *weight;
@property NSString *life_expectancy;
@property NSString *barking_level;

@end
