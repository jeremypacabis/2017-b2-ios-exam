//
//  AppDelegate.h
//  Dog Breeds Viewer
//
//  Created by ingenuity on 09/08/2017.
//  Copyright © 2017 ingenuity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

