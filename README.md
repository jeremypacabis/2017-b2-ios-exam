# IOS DEV. EXAM #

### DOG BREEDS VIEWER ###
This app will show different dog breeds with its corresponding photo and details

### Tasks ###

1. The app should be able to import the data (dog breeds, info) on the provided JSON file. This should only be done once by the app (which means that the app should not constantly load data from the JSON file every now and then).
2. The app should be able to display a list of all dog breeds, arranged in a tiled fashion, with only the thumbnail of each breed showing.
3. If the user will tap on the image, a page that displays a bigger image will be shown, plus the corresponding details of the breed.
4. In this page, add a button which will redirect to a form which will let the user edit the dog breed information
5. In the list of dog breed page (tiled), there is an option to filter the list based on Shedding, Grooming, Trainability, and Barking Level
6. For bonus points, make sure that the given image file named favicon.jpg will be used as the icon of the app.
